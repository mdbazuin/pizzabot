package main

import (
	"log"
	"time"

	"flag"

	"strings"

	irc "github.com/fluffle/goirc/client"
)

func main() {
	channel := flag.String("channel", "#kreenpananas", "Channel for twitch")
	user := flag.String("user", "", "Username for twitch")
	oauth := flag.String("oauth", "", "OAuth key for twitch user")
	delay := flag.Int("delay", 180, "Delay in minutes")
	flag.Parse()

	if strings.EqualFold(*user, "") {
		log.Fatal("User is not given. use -user")
	}

	if strings.EqualFold(*oauth, "") {
		log.Fatal("oauth is not given. use -oauth")
	}

	config := irc.NewConfig(*user)
	config.Pass = *oauth
	config.Server = "irc.chat.twitch.tv:6667"
	config.SSL = false

	client := irc.Client(config)

	quit := make(chan bool)
	timerQuit := make(chan bool)

	client.HandleFunc(irc.CONNECTED,
		func(conn *irc.Conn, line *irc.Line) {
			conn.Join(*channel)
			if conn.Connected() {
				log.Printf("Connected to %s\n", *channel)
			}
			go RepeatingMessage("!pizza", *delay, *channel, conn, timerQuit)
		})

	client.HandleFunc(irc.DISCONNECTED,
		func(conn *irc.Conn, line *irc.Line) {
			log.Println("Quitting")
			timerQuit <- true
			quit <- true
		})

	if err := client.Connect(); err != nil {
		log.Printf("Connection error: %s\n", err.Error())
	}

	<-quit
}

// RepeatingMessage ...
func RepeatingMessage(msg string, delay int, channel string, conn *irc.Conn, quit chan bool) {
	sendMsg(msg, channel, conn)
	ticker := time.NewTicker((time.Duration(delay) * time.Minute) + (100 * time.Millisecond))
	go func() {
		for {
			select {
			case <-ticker.C:
				sendMsg(msg, channel, conn)
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}

func sendMsg(msg, channel string, conn *irc.Conn) {
	conn.Privmsg(channel, "The following message is automated")
	time.Sleep(100 * time.Millisecond)
	conn.Privmsg(channel, msg)
}
